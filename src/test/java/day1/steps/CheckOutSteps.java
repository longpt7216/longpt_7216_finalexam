package day1.steps;

import day1.Utils.Util;
import day1.objects.Address;
import day1.objects.Product;
import day1.pages.CheckOutPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CheckOutSteps {

    public static String FULL_ADDRESS_STREET;

    @Steps
    CheckOutPage checkOutPage;

    public void verifyInfoOnCheckOutPage(Address newAddress, List<Product> inCartProducts) {
        verifyBuyerAddressOnCheckOutPage(newAddress);
        verifyProductInfoOnCheckOutPage(inCartProducts);
    }

    @Step
    public void verifyBuyerAddressOnCheckOutPage(Address newAddress){


        FULL_ADDRESS_STREET =
                newAddress.getAddress() + ", " +
                        newAddress.getWard() + ", " +
                        newAddress.getDistrict() + ", " +
                        newAddress.getCity();

        assertThat(checkOutPage.getAddressName()).isEqualTo(newAddress.getName());
        assertThat(checkOutPage.getAddressStreet()).isEqualTo(FULL_ADDRESS_STREET);
        assertThat(checkOutPage.getAddressPhone()).isEqualTo(newAddress.getTelephone());
    }

    @Step
    public void verifyProductInfoOnCheckOutPage(List<Product> inCartProducts) {
        checkOutPage.waits.Wait();

        long totalPrice = 0;

        for (int row = 1; row <= inCartProducts.size(); row++) {

            for (Product newProduct : inCartProducts) {
                String ob_PRODUCT_NAME = newProduct.getProductName() + " - " + newProduct.getProductSize();
                String ob_PRODUCT_REAL_PRICE = Util.intToVnD(newProduct.getProductPrice());

                if(checkOutPage.getProductName(row).equals(ob_PRODUCT_NAME)){
                    assertThat(checkOutPage.getProductName(row)).isEqualTo(ob_PRODUCT_NAME);
                    assertThat(checkOutPage.getProductRealPrice(row)).isEqualTo(ob_PRODUCT_REAL_PRICE + " ₫");
                    assertThat(checkOutPage.getProductQuantity(row)).isEqualTo("SL: x" + newProduct.getProductQuantity());
                    totalPrice += newProduct.getProductPrice() * newProduct.getProductQuantity();
                }
            }
        }


        assertThat(checkOutPage.getProductTempAfterPrice()).isEqualTo(Util.intToVnD(totalPrice) + "đ");

    }
}
