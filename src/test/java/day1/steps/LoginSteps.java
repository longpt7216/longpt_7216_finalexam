package day1.steps;

import day1.pages.CommonPage.HeaderPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class LoginSteps {
    @Steps
    HeaderPage loginPage;

    @Step
    public void iLoginToHomePage() {
        loginPage.open();

        loginPage.clickLoginRegistButton()
                .ClickLoginButton();
    }



}
