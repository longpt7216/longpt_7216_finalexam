package day1.steps;

import day1.pages.facebookPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class FacebookSteps {

    @Steps
    facebookPage facebookPage;

    @Step
    public void iLoginToHomePage() {
        facebookPage.openUrl("https://www.facebook.com/");


        facebookPage
                .enterAccount()
                .ClickLoginButton();
    }
}
