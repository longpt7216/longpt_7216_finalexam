package day1.steps;

import day1.pages.ProductPage;
import net.thucydides.core.annotations.Steps;

public class ProductSteps {

    @Steps
    ProductPage productPage;

    public ProductSteps iChooseTheProductColor(String color){

        productPage.iChooseProductDetail(color);
        return this;
    }

    public ProductSteps iChooseTheProductQuantity(int quantity){
        productPage.enterQuantityTextBox(quantity);
        return this;
    }

    public ProductSteps iClickBuyButton(){
        productPage.iClickBuyButton();
        return this;
    }


    public void iGoToChangeAddressDisplay(String page) {
        productPage.clickAPageFromProductPage(page);
    }
}
