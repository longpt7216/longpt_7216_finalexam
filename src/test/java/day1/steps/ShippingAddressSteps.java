package day1.steps;

import day1.Utils.GetData;
import day1.objects.Address;
import day1.pages.ShippingAddressPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

public class ShippingAddressSteps {



    public static
    GetData getData;

    public static Address newAddress;
    public static Address[] addresses;

    static {
        try {
            addresses = getData.getAddressData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step
    public ShippingAddressSteps iGetAddressDataFromJackSon(String addressName) {
        for (Address a : addresses) {
            if (a.getName().equals(addressName)) {
                newAddress = a;
                break;
            }
        }
        return this;
    }


    @Steps
    ShippingAddressPage shippingAddressPage;

    public void iDoSomethingOnShippingAddressDisplay(String action) {
        shippingAddressPage.iDoSomethingOnShippingAddressDisplay(action);
    }
}
