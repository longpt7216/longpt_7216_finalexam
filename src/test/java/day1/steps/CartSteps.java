package day1.steps;

import day1.Utils.Util;
import day1.objects.Product;
import day1.pages.CartPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CartSteps {
    @Steps
    CartPage cartPage;

    @Step
    public void iRemoveAllProduct() {
        cartPage.iRemoveAllProduct();
    }

    @Step
    public void iVerifyProductInfo(List<Product> inCarProducts) {
        cartPage.
                iClickSelectProductCheckbox();

        iVerifyProductDetailInfo(inCarProducts);



    }

    public void iClickBuyButtonInCartPage() {
        cartPage.iClickBuyButton();
    }


    @Step
    public void iVerifyProductDetailInfo(List<Product> inCarProducts){
        int row = inCarProducts.size();
        long totalPrice = 0;
        for (Product newProduct : inCarProducts) {

            String ob_PRODUCT_REAL_PRICE = Util.intToVnD(newProduct.getProductPrice() );
            String ob_PRODUCT_AFTER_PRICE = Util.intToVnD(newProduct.getProductPrice() * newProduct.getProductQuantity());

            if(!newProduct.getProductSize().isEmpty()) assertThat(cartPage.getProductName(row)).isEqualTo(newProduct.getProductName() + " - " + newProduct.getProductSize());

            assertThat(cartPage.getProductRealPrice(row)).isEqualTo(ob_PRODUCT_REAL_PRICE + " ₫");

            assertThat(cartPage.getProductAfterPrice(row)).isEqualTo(ob_PRODUCT_AFTER_PRICE + " ₫");
            assertThat(cartPage.getProductQuantity(row)).isEqualTo(Integer.toString(newProduct.getProductQuantity()));

            totalPrice += newProduct.getProductPrice() * newProduct.getProductQuantity();
            row--;
        }

        assertThat(cartPage.getProductTempAfterPrice()).isEqualTo(Util.intToVnD(totalPrice) + "đ");



    }
}
