package day1.steps;

import day1.Utils.GetData;
import day1.objects.Product;
import day1.pages.AllProductPage;
import day1.pages.CommonPage.LeftSideMenuListPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AllProductSteps {


    public static
    GetData getData;

    public static Product newProduct;
    public static Product[] products;
    public static List<Product> inCartProducts = new ArrayList<Product>();

    static {
        try {
            products = getData.getData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step
    public AllProductSteps iGetDataFromJackSon(String productID) {
        for (Product p : products) {
            if (p.getProductID().equals(productID)) {
                newProduct = p;
                inCartProducts.add(p);
                break;
            }
        }
        return this;
    }


    @Steps
    LeftSideMenuListPage leftSideMenuListPage;

    @Steps
    AllProductPage allProductPage;

    public void iChooseACatelogy(String catelogy) {
        leftSideMenuListPage.iChooseACatelogy(catelogy);
    }

    public void iChooseAProduct(String productName) {
        allProductPage.iChooseAProduct(productName);
    }
}
