package day1.base;

import io.cucumber.java.*;


public class Hooks {
    public static String currentlyExecutingScenario;

    @BeforeAll
    public static void beforeAll() {
        // Runs before all scenarios
        System.out.println("Start");
    }

    @AfterAll
    public static void afterAll() {
        // Runs before all scenarios
        System.out.println("End");
    }

    @Before
    public void getCurrentlyExecutingScenario(Scenario scenario){currentlyExecutingScenario = scenario.getName();}

}
