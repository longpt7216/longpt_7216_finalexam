package day1.base;

import day1.Utils.Util;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.List;

import java.time.Duration;

public class BasePage extends PageObject{
    public Util waits;
    private static WebDriverWait wait;

    public static void wait(int second) {
        try {
            Thread.sleep(second * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public WebElementFacade getElement(WebElementFacade element) {
        colorElement(element);
        element.waitUntilVisible();
        return element;
    }


    public WebElement getElement(String xpath) {
        wait = new WebDriverWait(getDriver(), Duration.ofSeconds(5));
        WebElement element = getDriver().findElement(By.xpath(xpath));
        colorElement(element);
        return wait.until(ExpectedConditions
                .presenceOfElementLocated(By.xpath(xpath)));
    }



    public BasePage waitUntilElementClickAbleAndClick(WebElementFacade element) {
        element.waitUntilClickable();
        colorElement(element);
        element.click();
        return this;
    }

    public BasePage waitUntilElementClickAbleAndClick(WebElement element) {
        wait = new WebDriverWait(getDriver(), Duration.ofSeconds(5));
        wait.until(ExpectedConditions
                .elementToBeClickable(element));
        colorElement(element);
        element.click();
        return this;
    }

    public void colorElement(WebElementFacade element){
        evaluateJavascript("arguments[0].setAttribute('style', 'background: orange; border: 2px solid red;');", element);
    }

    public void colorElement(WebElement element){
        evaluateJavascript("arguments[0].setAttribute('style', 'background: orange; border: 2px solid red;');", element);
    }


    public void enterTextIntoTxtBox(String text, WebElementFacade element) {
        waits.Wait();
        element.clear();
        colorElement(element);
        element.waitUntilClickable().type(text);
    }

}
