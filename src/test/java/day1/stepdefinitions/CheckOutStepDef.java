package day1.stepdefinitions;

import day1.steps.AllProductSteps;
import day1.steps.CheckOutSteps;
import day1.steps.ShippingAddressSteps;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CheckOutStepDef {

    @Steps
    CheckOutSteps checkOutSteps;

    @Then("i verify the address , product info and the price is correctly display in check out page")
    public void iVerifyTheAddressProductInfoAndThePriceIsCorrectlyDisplayInCheckOutPage() {
        checkOutSteps.verifyInfoOnCheckOutPage(ShippingAddressSteps.newAddress, AllProductSteps.inCartProducts);
    }

}
