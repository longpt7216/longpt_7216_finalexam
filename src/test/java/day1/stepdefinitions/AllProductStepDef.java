package day1.stepdefinitions;

import day1.steps.AllProductSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static day1.steps.AllProductSteps.newProduct;

public class AllProductStepDef {

    @Steps
    AllProductSteps allProductSteps;

    @Then("i choose {string} by the left side menu list")
    public void iChooseByTheLeftSideMenuList(String catelogy) {
        allProductSteps.iChooseACatelogy(catelogy);
    }

    @And("i choose a product name {string}")
    public void iChooseProductName(String productID) {
        allProductSteps.
                iGetDataFromJackSon(productID).
                iChooseAProduct(newProduct.getProductName());
    }

    @And("i choose detail for {string} and add to Cart")
    public void iChooseDetailForAndAddToCart(String numberOfProduct) {
        //
        // SOlUTION 2, Bỏ qua ko làm .
        // loop qua jSon Data file và lấy bao nhiều dữ liệu tương ứng từ 1~n
    }
}
