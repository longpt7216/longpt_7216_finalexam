package day1.stepdefinitions;

import day1.steps.ProductSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static day1.steps.AllProductSteps.newProduct;

public class ProductStepDef {

    @Steps
    ProductSteps productSteps;

    @And("i choose detail for the product")
    public void iChooseDetailForTheProduct() {
        if(!newProduct.getProductSize().isEmpty())
        productSteps.
                iChooseTheProductColor(newProduct.getProductSize()).
                iChooseTheProductQuantity(newProduct.getProductQuantity());

    }

    @Then("i add product to Cart")
    public void iClickBuyButton() {
        productSteps.iClickBuyButton();
    }


    @Given("i go to {string} page from Product page")
    public void iGoToPageFromProductPage(String page) {
        productSteps.iGoToChangeAddressDisplay(page);
    }
}
