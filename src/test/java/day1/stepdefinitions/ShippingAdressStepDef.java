package day1.stepdefinitions;

import day1.steps.ShippingAddressSteps;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ShippingAdressStepDef {

    @Steps
    ShippingAddressSteps shippingAddressSteps;


    @When("{string} the delivery address to the address of {string}")
    public void theDeliveryAddressToTheAddressOf(String action , String addressName) {
        switch (action) {
            case "change" :
                shippingAddressSteps.iGetAddressDataFromJackSon(addressName);
                shippingAddressSteps.iDoSomethingOnShippingAddressDisplay("change");
                shippingAddressSteps.iDoSomethingOnShippingAddressDisplay("edit address");
                break;
            default:
                System.out.println("DO NOTHING");
        }
    }
}
