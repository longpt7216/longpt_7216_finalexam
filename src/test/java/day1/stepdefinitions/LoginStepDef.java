package day1.stepdefinitions;


import day1.steps.CartSteps;
import day1.steps.FacebookSteps;
import day1.steps.HomeSteps;
import day1.steps.LoginSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class LoginStepDef extends ScenarioSteps {


    @Steps
    LoginSteps loginSteps;

    @Steps
    FacebookSteps facebookSteps;

    @Steps
    HomeSteps homeSteps;

    @Steps
    CartSteps cartSteps;


    @Given("i login to Tiki \\(and clear the Cart if there are products in it)")
    public void iLoginToTikiAndClearTheCartIfThereAreProductsInIt() {
        facebookSteps.iLoginToHomePage();
        loginSteps.iLoginToHomePage();
        homeSteps.goToPage("Cart");
        cartSteps.iRemoveAllProduct();
    }

}
