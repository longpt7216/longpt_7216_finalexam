package day1.stepdefinitions;

import day1.steps.HomeSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class HomeStepDef extends ScenarioSteps {


    @Steps
    HomeSteps homeSteps;


    @Given("i go to {string} page")
    public void iGoToPage(String page) {
        homeSteps.goToPage(page);
    }



}
