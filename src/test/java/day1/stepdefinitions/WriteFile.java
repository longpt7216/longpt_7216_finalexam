package day1.stepdefinitions;

import com.github.javafaker.Address;
import com.github.javafaker.Faker;
import com.github.javafaker.Internet;
import com.github.javafaker.Number;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import day1.Utils.LoadConfig;
import day1.Utils.ObjectToJsonFile;
import day1.Utils.Util;
import day1.Utils.WriteDataToExcel;
import day1.objects.DataTest;
import io.cucumber.java.en.Given;

import java.io.IOException;
import java.util.*;

public class WriteFile {

    Faker faker = new Faker(new Locale("ja_JP")); // 日本語対応

    public static ArrayList<DataTest> dataTests = new ArrayList<>();


    @Given("i add random object")
    public void iAddRandomObject() throws IOException {


//        https://www.whizz-tech.co.jp/2877/

        int WriteDataQuantity = Integer.parseInt(LoadConfig.getWriteDataQuantity());
        for (int i = 0; i < WriteDataQuantity; i++) {
            DataTest dataTest = new DataTest();
            dataTest.setName(getName());
            dataTest.setAge(getAge());
            dataTest.setSex(getSex());
            dataTest.setHeight(randomDouble(2,1,2));
            dataTest.setWeight(getWeight());
            dataTest.setBloodType(getBloodType());
            dataTest.setWork(getJob());
            dataTest.setPostNumber(getPostNumber());
            dataTest.setAddress(getAddress());
            dataTest.setMail(getEmail());
            dataTest.setTelephone(getTelephoneNumber());
            dataTest.setWorkplace(getAddress());
            dataTest.setWebsite(getWebsite());
            dataTest.setExp(getExp());
            dataTest.setCompanyID(getCompanyID());
            dataTest.setContractCondition(getContractCondition());
            dataTest.setComment(getContractCondition());
            dataTests.add(dataTest);
        }



        WriteDataToFile(LoadConfig.getWriteDataFileType());


    }

    public void WriteDataToFile(String writeDataFileType) throws IOException {
        switch(writeDataFileType){
            case "csv" :
                Util.writeToCSV(dataTests);
                break;
            case "json" :
                ObjectToJsonFile.ObjectToJsonFile(dataTests);
                break;
            case "excel" :
                WriteDataToExcel.WriteDataToExcel(dataTests);
        }
    }


    public String getName(){
//        System.out.println("fullName：" + faker.name().fullName()); // フルネーム
        return faker.name().fullName();
    }

    public Integer getAge(){
        // randomを利用する方法
        RandomService random = faker.random();
        return random.nextInt(0, 150); // 指定範囲内のランダムなInteger
    }

    public String getPhoneNumber(){
        return faker.phoneNumber().cellPhone();
    }

    public String getBloodType(){
                List<Object> bloodType = new ArrayList<>();
        Collections.addAll(bloodType, "A", "B", "O");
            return (String) bloodType.get(new Random().nextInt(bloodType.size()));

    }

    public String getSex(){
        List<Object> sex = new ArrayList<>();
        Collections.addAll(sex, "男", "女", "その他");
        return (String) sex.get(new Random().nextInt(sex.size()));
    }

    public double randomDouble(int maxNumberOfDecimals, int min, int max) {

//        System.out.println("number().randomDouble(2, 5, 10)：" + String.valueOf(faker.number().randomDouble(2, 5, 10)));
        return faker.number().randomDouble(maxNumberOfDecimals, min, max);
    }

    public Integer getWeight(){
        // randomを利用する方法
        RandomService random = faker.random();
        return random.nextInt(30, 120); // 指定範囲内のランダムなInteger
    }

    public String getJob(){
        List<Object> jobType = new ArrayList<>();
        Collections.addAll(jobType
                ,"	政府連絡係"
                ,"マーケティングファシリテーター"
                ,"政府技術者"
                ,"製品法務アソシエイト"
                ,"ナショナル バンキング デベロッパー"
                ,"政府補佐官"
                ,"営業部長"
                ,"設計監修"
                ,"ダイナミックコンサルタント"
                ,"国家戦略家"
                ,"従来のセールス アーキテクト"
                ,"カスタマー ファーミング ストラテジスト"
                ,"政府の建築家"
                ,"コンサルティング代表"
                ,"教育アシスタント"
                ,"政府コンサルタント"
                ,"地域マーケティング デベロッパー"
                ,"デザイン エグゼクティブ"
                ,"教育スペシャリスト"
                ,"主任コンサルタント"
        );
        return (String) jobType.get(new Random().nextInt(jobType.size()));



    }


    public String getPostNumber(){
        Address address = faker.address();
//        System.out.println("zipCode：" + address.zipCode()); // 郵便番号
        return address.zipCode();
    }

    public String getAddress(){
        Address address = faker.address();
//        System.out.println("zipCode：" + address.zipCode()); // 郵便番号
//        System.out.println("stateAbbr：" + address.stateAbbr()); // 都道府県コード
//        System.out.println("state：" + address.state()); // 都道府県
//        System.out.println("cityName：" + address.cityName()); // 市
//        System.out.println("streetAddress：" + address.streetAddress()); // 通り住所
//        System.out.println("secondaryAddress：" + address.secondaryAddress()); // その他住所？

// 全部繋げて住所にする
//        System.out.println("繋げてみた住所：" + address.zipCode() + address.state() + address.city() + address.cityName() + address.streetAddress(true));
        return  address.state() + address.city() + address.cityName();

    }


    public String getEmail(){

        FakeValuesService fakeValuesService = new FakeValuesService(
                new Locale("ja_JP"), new RandomService());

        Internet internet = faker.internet();

        // 引数無しの場合は日本語アカウントになってしまう
//        System.out.println("safeEmailAddress：" + internet.safeEmailAddress());

        // fakeValuesService.bothify("???_###")を利用して適当なアドレスを生成（?は文字、#は数値）
//        System.out.println("safeEmailAddress：" + internet.safeEmailAddress(fakeValuesService.bothify("???_###")));
        return internet.safeEmailAddress(fakeValuesService.bothify("???_###"));
    }
    public String getTelephoneNumber(){
//        System.out.println("phoneNumber.cellPhone：" + faker.phoneNumber().cellPhone());
        return faker.phoneNumber().cellPhone();
    }

    public String getWebsite(){
//        System.out.println("internet().url()：" + faker.internet().url());
        return faker.internet().url();
    }

    public int getExp(){
        // randomを利用する方法
        RandomService random = faker.random();
        return random.nextInt(1, 50); // 指定範囲内のランダムなInteger
    }

    public String getContractCondition(){
//        System.out.println("shakespeare().asYouLikeItQuote()：" + faker.shakespeare().asYouLikeItQuote());
//        System.out.println("shakespeare().hamletQuote()：" + faker.shakespeare().hamletQuote());
//        System.out.println("shakespeare().kingRichardIIIQuote()：" + faker.shakespeare().kingRichardIIIQuote());
//        System.out.println("shakespeare().romeoAndJulietQuote()：" + faker.shakespeare().romeoAndJulietQuote());

        return faker.shakespeare().asYouLikeItQuote();
    }

    public String getCompanyID(){
        Number number = faker.number();
//        System.out.println(number.digits(5)); // 指定桁数の数字文字列
        String first = number.digits(5);
        String sec = number.digits(4);
        String third = number.digits(6);
        return first + "-" + sec + "-" + third;
    }
}








