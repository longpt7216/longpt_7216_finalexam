package day1.stepdefinitions;

import day1.steps.AllProductSteps;
import day1.steps.CartSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;


public class CartStepDef {

    @Steps
    CartSteps cartSteps;

    @And("i remove existing product in Cart")
    public void RemoveExistingProductInCart() {
        cartSteps.iRemoveAllProduct();
    }

    @And("i click buy button in Cart page")
    public void iClickBuyButtonInCartPage() {
        cartSteps.iClickBuyButtonInCartPage();
    }


    @Then("i verify the product info and the price is correct")
    public void iVerifyTheProductInfoAndThePriceIsCorrect() {
        cartSteps.iVerifyProductInfo(AllProductSteps.inCartProducts);
    }
}
