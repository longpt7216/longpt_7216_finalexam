package day1.stepdefinitions;

import day1.steps.FacebookSteps;
import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class FacebookStepDef {

    @Steps
    FacebookSteps facebookSteps;

    @Given("i login to Facebook")
    public void iLoginToHomePage() {
        facebookSteps.iLoginToHomePage();
    }
}
