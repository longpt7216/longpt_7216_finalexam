package day1.Utils;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class LoadConfig {
    final static EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();

    public static String getDriver() {return EnvironmentSpecificConfiguration.from(env).getProperty("webdriver.driver");}

    public static String getFacebookUsername() {return EnvironmentSpecificConfiguration.from(env).getProperty("facebookAccount.fb_username");}

    public static String getFacebookPassword() {return EnvironmentSpecificConfiguration.from(env).getProperty("facebookAccount.fb_password");}

    public static String getProductDataPath() {return EnvironmentSpecificConfiguration.from(env).getProperty("ProductDataPath");}

    public static String getAddressDataPath() {return EnvironmentSpecificConfiguration.from(env).getProperty("AddressDataPath");}

    public static String getWriteDataQuantity() {return EnvironmentSpecificConfiguration.from(env).getProperty("WriteDataQuantity");}
    public static String getWriteDataFileType() {return EnvironmentSpecificConfiguration.from(env).getProperty("OutPutFileType");}
}
