package day1.Utils;

import day1.objects.Address;
import day1.objects.Product;

import java.io.IOException;

public class GetData {

    public static Product[] getData() throws IOException {return Util.deserializeJson(LoadConfig.getProductDataPath(), Product[].class);}

    public static Address[] getAddressData() throws IOException {return Util.deserializeJson(LoadConfig.getAddressDataPath(), Address[].class);}


}
