package day1.Utils;
import java.io.File;

import day1.objects.DataTest;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class WriteDataToExcel {

    //https://www.geeksforgeeks.org/how-to-write-data-into-excel-sheet-using-java/


    // any exceptions need to be caught
    public static void WriteDataToExcel(ArrayList<DataTest> dataTests) throws IOException {
        // workbook object
        XSSFWorkbook workbook = new XSSFWorkbook();

        // spreadsheet object
        XSSFSheet spreadsheet
                = workbook.createSheet(" Test Data ");

        // creating a row object
        XSSFRow row;

        // This data needs to be written (Object[])
        Map<String, Object[]> testDataMap
                = new TreeMap<String, Object[]>();

        testDataMap.put( "1", new Object[] {
                "名前", "年齢", "性別", "身長", "体重", "血液型", "仕事", "郵便番号", "住所", "メールアドレス", "電話番号", "勤務先", "webサイト", "経験年数", "会社ID", "契約条件", "コメント"});

        int no = 2;
        for (DataTest dataTest : dataTests) {
            testDataMap.put(
                    String.valueOf(no),
                    new Object[] {
                            dataTest.getName(),
                            String.valueOf(dataTest.getAge()),
                            dataTest.getSex() ,
                            String.valueOf(dataTest.getHeight()),
                            String.valueOf(dataTest.getWeight()),
                            dataTest.getBloodType(),
                            dataTest.getWork(),
                            dataTest.getPostNumber(),
                            dataTest.getAddress(),
                            dataTest.getMail(),
                            dataTest.getTelephone(),
                            dataTest.getWorkplace(),
                            dataTest.getWebsite(),
                            String.valueOf(dataTest.getExp()),
                            dataTest.getCompanyID(),
                            dataTest.getContractCondition(),
                            dataTest.getComment()
                    });
            no++;
        }



        Set<String> keyid = testDataMap.keySet();

        int rowid = 0;

        // writing the data into the sheets...

        for (String key : keyid) {

            row = spreadsheet.createRow(rowid++);
            Object[] objectArr = testDataMap.get(key);
            int cellid = 0;

            for (Object obj : objectArr) {
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String)obj);
            }
        }

        // .xlsx is the format for Excel Sheets...
        // writing the workbook into the file...
        FileOutputStream out = new FileOutputStream("DataTest.xlsx");

        workbook.write(out);
        out.close();
    }
}