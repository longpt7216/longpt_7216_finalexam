package day1.Utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import day1.objects.DataTest;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;

public class Util extends PageObject {

    public static <T> T deserializeJson(String fileName, Class<T> T) throws IOException {
        InputStream is = Util.class.getClassLoader().getResourceAsStream(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(is, T);
    }


    public static String getCurrentTime(){
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        return dateFormat.format(date);
    }

    public void Wait() {
        wHTML(100);
        wJQry(60);
        wJQryRequest(30);
    }

    public void wHTML(int timeoutInSeconds) {
        new FluentWait<>(getDriver()).withTimeout(Duration.of(timeoutInSeconds, SECONDS))
                .pollingEvery(Duration.of(100, ChronoUnit.MILLIS)).until((ExpectedCondition<Boolean>) d -> {
                    try {
                        JavascriptExecutor jsExec = (JavascriptExecutor) d;
                        assert jsExec != null;
                        return jsExec.executeScript("return document.readyState").toString()
                                .equals("complete");
                    } catch (Exception e) {
                        return true;
                    }
                });
    }

    public void wJQry(int timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(timeoutInSeconds));
        ExpectedCondition<Boolean> jQueryLoad = driver -> {
            try {
                assert driver != null;
                return ((Long) ((JavascriptExecutor) driver).executeScript("return $.active") == 0);
            } catch (Exception e) {
                return true;
            }
        };
        wait.until(jQueryLoad);
    }

    public void wJQryRequest(int timeoutInSeconds) {
        try {
            new FluentWait<>(getDriver()).withTimeout(Duration.of(timeoutInSeconds, SECONDS))
                    .withMessage("**** INFO **** JQUERY STILL LOADING FOR OVER" + timeoutInSeconds + "SECONDS.")
                    .pollingEvery(Duration.of(100, ChronoUnit.MILLIS)).until((ExpectedCondition<Boolean>) d -> {
                        try {
                            JavascriptExecutor jsExec = (JavascriptExecutor) d;
                            assert jsExec != null;
                            return (Boolean) jsExec.executeScript("return jQuery.active == 0");
                        } catch (Exception e) {
                            return true;
                        }
                    });
        } catch (Exception ignored) {
        }
    }

    public static String intToVnD(long money){
        DecimalFormat formatter = new DecimalFormat("###,###,###");

        return formatter.format(money).replace(",", ".");
    }


    //European countries use ";" as
    //CSV separator because "," is their digit separator
    private static final String CSV_SEPARATOR = ",";
    public static void writeToCSV(ArrayList<DataTest> dataTests)


    {
        try
        {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("DataTest.csv"), "UTF-8"));
            for (DataTest dataTest : dataTests)
            {
                StringBuffer oneLine = new StringBuffer();

                oneLine.append(dataTest.toString());
//                oneLine.append(dataTest.getName());
//                oneLine.append(CSV_SEPARATOR);
//                oneLine.append(dataTest.getAge());
//                oneLine.append(CSV_SEPARATOR);
//                oneLine.append(dataTest.getCostPrice() < 0 ? "" : dataTest.getCostPrice());
//                oneLine.append(CSV_SEPARATOR);
//                oneLine.append(dataTest.isVatApplicable() ? "Yes" : "No");
                bw.write(oneLine.toString());
                bw.newLine();
            }
            bw.flush();
            bw.close();
        }
        catch (UnsupportedEncodingException e) {}
        catch (FileNotFoundException e){}
        catch (IOException e){}
    }


}
