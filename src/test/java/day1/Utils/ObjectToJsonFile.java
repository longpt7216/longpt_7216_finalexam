package day1.Utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import day1.objects.DataTest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ObjectToJsonFile {

//    https://kodejava.org/how-to-read-and-write-java-object-to-json-file/

    public static void ObjectToJsonFile(ArrayList<DataTest> dataTests) {




        ObjectMapper mapper = new ObjectMapper();

        File file = new File("DataTest.json");
        try {
//             Serialize Java object info JSON file.
            mapper.writeValue(file, dataTests);


//            for (DataTest dataTest : dataTests) {
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }

//        try {
//            // Deserialize JSON file into Java object.
//            Artist newArtist = mapper.readValue(file, Artist.class);
//            System.out.println("newArtist.getId() = " + newArtist.getId());
//            System.out.println("newArtist.getName() = " + newArtist.getName());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
}