package day1.pages;

import day1.Utils.Util;
import day1.base.BasePage;
import day1.objects.Product;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;

import static day1.steps.AllProductSteps.inCartProducts;
import static day1.steps.ShippingAddressSteps.newAddress;
import static org.assertj.core.api.Assertions.assertThat;

public class CheckOutPage extends BasePage {

    public static String PRODUCT_NAME, PRODUCT_REAL_PRICE, PRODUCT_QUANTITY, PRODUCT_TEMP_AFTER_PRICE;


//    @FindBy(xpath = "//span[contains(@class,'product-name')]") private WebElementFacade E_PRODUCT_NAME;
//    @FindBy(xpath = "(//div[@class='item-info__qty'])[1]") private WebElementFacade E_PRODUCT_QUANTITY;
//    @FindBy(xpath = "//div[@class='item-info__price']") private WebElementFacade E_PRODUCT_REAL_PRICE;

    @FindBy(xpath = "//div[@class='summary-label'][contains(.,'Tạm tính')]//following-sibling::div") private WebElementFacade E_PRODUCT_TEMP_AFTER_PRICE;


    @FindBy(xpath = "//p[@class='customer_info__name']") private WebElementFacade E_ADDRESS_NAME;
    @FindBy(xpath = "//div[@class='address']") private WebElementFacade E_ADDRESS_STREET;
    @FindBy(xpath = "//p[@class='customer_info__phone']") private WebElementFacade E_ADDRESS_PHONE;

    private static final String xpath_E_PRODUCT_NAME = "(//span[@class='item-info__product-name'])[%d]";
    private static final String xpath_PRODUCT_REAL_PRICE = "(//div[@class='item-info__price'])[%d]";
    private static final String xpath_PRODUCT_QUANTITY = "(//div[@class='item-info__qty'])[%d]";





    public String getAddressName() {
        return getElement(E_ADDRESS_NAME).getText();
    }

    public String getAddressStreet() {
        return getElement(E_ADDRESS_STREET).getText();
    }

    public String getAddressPhone() {
        return getElement(E_ADDRESS_PHONE).getText();
    }

    public String getProductName(int row) {
        return getElement(String.format(xpath_E_PRODUCT_NAME, row)).getText();
    }

    public String getProductRealPrice(int row) {
        return getElement(String.format(xpath_PRODUCT_REAL_PRICE, row)).getText();
    }

    public String getProductQuantity(int row) {
        return getElement(String.format(xpath_PRODUCT_QUANTITY, row)).getText();
    }

    public String getProductTempAfterPrice() {
        return getElement(E_PRODUCT_TEMP_AFTER_PRICE).getText();
    }
}
