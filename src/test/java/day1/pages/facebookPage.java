package day1.pages;

import day1.Utils.LoadConfig;
import day1.base.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;

public class facebookPage extends BasePage {
    @FindBy(xpath = "//*[@id='email']") private WebElementFacade E_FB_USERNAME;
    @FindBy(xpath = "//*[@id='pass']") private WebElementFacade E_FB_PASSWORD;


    @Step
    public facebookPage enterAccount(){
        waits.Wait();
        E_FB_USERNAME.sendKeys(LoadConfig.getFacebookUsername());
        E_FB_PASSWORD.sendKeys(LoadConfig.getFacebookPassword());
        return this;
    }

    @Step
    public facebookPage ClickLoginButton(){
        E_FB_PASSWORD.submit();
        waits.Wait();
        return null;
    }
}
