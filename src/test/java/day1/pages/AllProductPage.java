package day1.pages;

import day1.base.BasePage;

public class AllProductPage extends BasePage {

    public void iChooseAProduct(String productName) {

        String xpath = "(//div[@class='name'][contains(.,'" +  productName + "')])[1]";

        waits.Wait();
        wait(2);
        waitUntilElementClickAbleAndClick(getElement(xpath));
    }
}
