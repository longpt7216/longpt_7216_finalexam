package day1.pages;

import day1.Utils.Util;
import day1.base.BasePage;
import day1.objects.Product;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;


import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CartPage extends BasePage {

    public static String PRODUCT_NAME, PRODUCT_REAL_PRICE, PRODUCT_AFTER_PRICE, PRODUCT_QUANTITY, PRODUCT_TEMP_AFTER_PRICE;



    @FindBy(xpath = "//*[@class='product__delete']") private WebElementFacade E_SINGLE_TRASHBINLOGO;
    @FindBy(xpath = "//div[@class='dialog-control__button dialog-control__button--secondary'][contains(.,'Xác Nhận')]") private WebElementFacade E_DELETE_BUTTON;

//    @FindBy(xpath = "(//a[@class='product__name'])[1]") WebElementFacade E_PRODUCT_NAME;
//    @FindBy(xpath = "(//span[@class='product__real-prices'])[1]") WebElementFacade E_PRODUCT_REAL_PRICE;
//    @FindBy(xpath = "(//span[@class='product__final-prices'])[1]") WebElementFacade E_PRODUCT_AFTER_PRICE;
    @FindBy(xpath = "//span[@class='prices__value']") WebElementFacade E_PRODUCT_TEMP_AFTER_PRICE;
//    @FindBy(xpath = "(//input[@class='qty-input'])[1]") WebElementFacade E_PRODUCT_QUANTITY;

    @FindBy(xpath = "(//span[@class='checkbox-fake'])[1]") private WebElementFacade E_PRODUCT_BUY_CHECKBOX;
    @FindBy(xpath = "//button[contains(.,'Mua Hàng')]") private WebElementFacade E_PRODUCT_BUY_BUTTON;


    private static final String xpath_E_PRODUCT_NAME = "(//a[@class='product__name'])[%d]";
    private static final String xpath_PRODUCT_REAL_PRICE = "(//span[@class='product__real-prices'])[%d]";
    private static final String xpath_PRODUCT_AFTER_PRICE = "(//span[@class='product__final-prices'])[%d]";
    private static final String xpath_PRODUCT_QUANTITY = "(//input[@class='qty-input'])[%d]";

    @Step
    public void iRemoveAllProduct() {
        wait(2);
        while (E_SINGLE_TRASHBINLOGO.isCurrentlyVisible()){
            waitUntilElementClickAbleAndClick(getElement(E_SINGLE_TRASHBINLOGO));
            waitUntilElementClickAbleAndClick(getElement(E_DELETE_BUTTON));
            wait(2);
        }
    }

    @Step
    public CartPage iVerifyProductInfo(List<Product> inCartProducts) {

        int row = inCartProducts.size();
        long totalPrice = 0;
        for (Product newProduct : inCartProducts) {

            String ob_PRODUCT_REAL_PRICE = Util.intToVnD(newProduct.getProductPrice() );
            String ob_PRODUCT_AFTER_PRICE = Util.intToVnD(newProduct.getProductPrice() * newProduct.getProductQuantity());

            if(!newProduct.getProductSize().isEmpty()) assertThat(PRODUCT_NAME).isEqualTo(newProduct.getProductName() + " - " + newProduct.getProductSize());
            assertThat(PRODUCT_REAL_PRICE).isEqualTo(ob_PRODUCT_REAL_PRICE + " ₫");

            assertThat(PRODUCT_AFTER_PRICE).isEqualTo(ob_PRODUCT_AFTER_PRICE + " ₫");
            assertThat(PRODUCT_QUANTITY).isEqualTo(Integer.toString(newProduct.getProductQuantity()));

            totalPrice += newProduct.getProductPrice() * newProduct.getProductQuantity();
            row--;
        }

        assertThat(PRODUCT_TEMP_AFTER_PRICE).isEqualTo(Util.intToVnD(totalPrice) + "đ");

        return this;
    }

    @Step
    public CartPage iClickSelectProductCheckbox() {
        getElement(E_PRODUCT_BUY_CHECKBOX);
        E_PRODUCT_BUY_CHECKBOX.click();
        return this;
    }

    @Step
    public CartPage iClickBuyButton() {
        getElement(E_PRODUCT_BUY_BUTTON);
        E_PRODUCT_BUY_BUTTON.click();
        return this;
    }
    public String getProductName(int row){
        return getElement(String.format(xpath_E_PRODUCT_NAME, row)).getText();
    }

    public String getProductRealPrice(int row){
        return getElement(String.format(xpath_PRODUCT_REAL_PRICE, row)).getText();
    }

    public String getProductAfterPrice(int row){
        return getElement(String.format(xpath_PRODUCT_AFTER_PRICE, row)).getText();
    }

    public String getProductQuantity(int row){
        return getElement(String.format(xpath_PRODUCT_QUANTITY, row)).getAttribute("value");
    }

    public String getProductTempAfterPrice(){
        return getElement(E_PRODUCT_TEMP_AFTER_PRICE).getText();
    }

}
