package day1.pages;

import day1.base.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {
    @FindBy(xpath = "//div[@class='qty-and-message']//input[contains(@class,'input')]") private WebElementFacade QUANTITY_TEXTBOX;
    @FindBy(xpath = "//button[@type='button'][contains(.,'Chọn mua')]") private WebElementFacade BUY_BUTTON;

    @FindBy(xpath = "//a[@class='block-header__nav'][contains(.,'Thay đổi')]") private WebElementFacade CHANGE_ADDRESS_BUTTON;

    @Step
    public void iChooseProductDetail(String detail) {
        String xpath = "//button[contains(.,'" +  detail + "')]";

        waits.Wait();
        wait(2);
        waitUntilElementClickAbleAndClick(getElement(xpath));
        wait(2);
        waits.Wait();
    }

    @Step
    public ProductPage enterQuantityTextBox(int quantity) {
        waits.Wait();
        wait(2);
        enterTextIntoTxtBox(String.valueOf(quantity), QUANTITY_TEXTBOX);
        waits.Wait();
        return this;
    }

    @Step
    public ProductPage iClickBuyButton() {
        wait(1);
        waitUntilElementClickAbleAndClick(BUY_BUTTON);
        waits.Wait();
        return this;
    }

    @Step
    public void clickAPageFromProductPage(String page){
        switch (page) {
            case "change address" :
                waitUntilElementClickAbleAndClick(CHANGE_ADDRESS_BUTTON);
                waits.Wait();
                break;
            default:
                System.out.println("Page Option Not Exist");
        }
    }
}
