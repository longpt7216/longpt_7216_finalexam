package day1.pages;

import day1.base.BasePage;
import day1.steps.ShippingAddressSteps;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class ShippingAddressPage extends BasePage {

    @FindBy(xpath = "(//button[@type='button'][contains(.,'Sửa')])[1]") private WebElementFacade CHANGE_ADDRESS_BUTTON;

    @FindBy(xpath = "//input[@name='full_name']") private WebElementFacade FULL_NAME_TEXTBOX;
    @FindBy(xpath = "//input[@name='telephone']") private WebElementFacade TELEPHONE_TEXTBOX;


    @FindBy(xpath = "(//div[contains(@class,'rc-select-selection__rendered')])[1]") private WebElementFacade CITY_TEXTBOX2;
    @FindBy(xpath = "(//div[contains(@class,'rc-select-selection__rendered')])[2]") private WebElementFacade DISTRICT_TEXTBOX2;
    @FindBy(xpath = "(//div[contains(@class,'rc-select-selection__rendered')])[3]") private WebElementFacade WARD_TEXTBOX2;
    @FindBy(xpath = "//textarea[@name='street']") private WebElementFacade STREET_TEXTBOX;
    @FindBy(xpath = "//label[contains(.,'Nhà riêng / Chung cư')]") private WebElementFacade HOME_LABEL;
    @FindBy(xpath = "//label[contains(.,'Cơ quan / Công ty')]")  private WebElementFacade COMPANY_LABEL;
    @FindBy(xpath = "//label[contains(.,'Sử dụng địa chỉ này làm mặc định.')]") private WebElementFacade DEFAULT_ADDRESS_LABEL;
    @FindBy(xpath = "//button[@class='create-update'][contains(.,'Cập nhật')]") private WebElementFacade UPDATE_ADDRESS_BUTTON;
    @FindBy(xpath = "(//button[@type='button'][contains(.,'Giao đến địa chỉ này')])[1]") private WebElementFacade CHOOSE_ADDRESS_BUTTON;



    public void iDoSomethingOnShippingAddressDisplay(String action) {
        waits.Wait();
        switch (action) {
            case "change" :
                waitUntilElementClickAbleAndClick(CHANGE_ADDRESS_BUTTON);
                break;
            case "edit address" :
                editAddressAndClickUpdateAddress();
                chooseAnAddress();
                break;
            default:
                System.out.println("Page Option Not Exist");
        }
    }

    private void chooseAnAddress() {
        waitUntilElementClickAbleAndClick(CHOOSE_ADDRESS_BUTTON);
    }


    public void editAddressAndClickUpdateAddress(){
        String xpath;

        enterTextIntoTxtBox(ShippingAddressSteps.newAddress.getName(), FULL_NAME_TEXTBOX);
        enterTextIntoTxtBox(ShippingAddressSteps.newAddress.getTelephone(), TELEPHONE_TEXTBOX);

        //not a select element but an ul li element, so i have to click
        xpath = "//li[@role='option'][contains(.,'" + ShippingAddressSteps.newAddress.getCity() + "')]";
        waitUntilElementClickAbleAndClick(CITY_TEXTBOX2).waitUntilElementClickAbleAndClick(getElement(xpath));

        xpath = "//li[@role='option'][contains(.,'" + ShippingAddressSteps.newAddress.getDistrict() + "')]";
        waitUntilElementClickAbleAndClick(DISTRICT_TEXTBOX2).waitUntilElementClickAbleAndClick(getElement(xpath));

        xpath = "//li[@role='option'][contains(.,'" + ShippingAddressSteps.newAddress.getWard() + "')]";
        waitUntilElementClickAbleAndClick(WARD_TEXTBOX2).waitUntilElementClickAbleAndClick(getElement(xpath));

        enterTextIntoTxtBox(ShippingAddressSteps.newAddress.getAddress(), STREET_TEXTBOX);

        xpath = "//label[contains(.,'" + ShippingAddressSteps.newAddress.getAdressType() + "')]";
        waitUntilElementClickAbleAndClick(getElement(xpath));

        if (ShippingAddressSteps.newAddress.isDefaultAddress()) {
            //vì chỉ có 1 địa chỉ nên mặc định là default address
            getElement(DEFAULT_ADDRESS_LABEL);
        }

        waitUntilElementClickAbleAndClick(UPDATE_ADDRESS_BUTTON);
        waits.Wait();

        waitForTextToDisappear("Họ tên" , 3000);
    }
}
