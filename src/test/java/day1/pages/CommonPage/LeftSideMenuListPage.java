package day1.pages.CommonPage;

import day1.base.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;

public class LeftSideMenuListPage extends BasePage {
    private WebElementFacade E_CATALOGY;


    public void iChooseACatelogy(String catelogy) {

        String xpath = "//a[@data-view-id='search_filter_item'][contains(.,'" +  catelogy + "')]";

        waits.Wait();
        wait(2);
        waitUntilElementClickAbleAndClick(getElement(xpath));
    }
}
