package day1.pages.CommonPage;

import day1.base.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;


public class HeaderPage extends BasePage{
    @FindBy(xpath = "//*[@class='Userstyle__NoWrap-sc-6e6am-12 gJAiTt']") private WebElementFacade E_LOGIN;

    @FindBy(xpath = "//*[@class='social__item'][1]") private WebElementFacade E_FB_LOGIN_BUTTON;


    @Step
    public HeaderPage clickLoginRegistButton(){
        waits.Wait();
        wait(1);
        waitUntilElementClickAbleAndClick(E_LOGIN);
        return this;
    }

    @Step
    public HeaderPage ClickLoginButton(){
        wait(2);
        while(E_FB_LOGIN_BUTTON.isCurrentlyVisible()) {
            if (E_FB_LOGIN_BUTTON.isClickable()){
                E_FB_LOGIN_BUTTON.click();
            }
            wait(4);
        }
        return null;
    }
}


