package day1.pages;

import day1.base.BasePage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.support.FindBy;

public class homePage extends BasePage {
    @FindBy(xpath = "//div[@class='style__StyledCateName-sc-x9fskq-1 fVbTIP'][contains(.,'Nhà Cửa')]") private WebElementFacade E_NHACUA;
    @FindBy(xpath = "//span[@class='cart-text'][contains(.,'Giỏ Hàng')]") private WebElementFacade E_CART;
    @FindBy(xpath = "//img[@alt='tiki-logo']") private WebElementFacade E_TIKI_LOGO;



    @Step
    public void clickAPageInHomePage(String page){
        switch (page) {
            case "Nhà Cửa" :
                waitUntilElementClickAbleAndClick(E_NHACUA);
                break;
            case "Cart" :
                waitUntilElementClickAbleAndClick(E_CART);
                break;
            case "Home" :
                waitUntilElementClickAbleAndClick(E_TIKI_LOGO);
                break;
            default:
                System.out.println("Page Option Not Exist");
        }
    }

}
