package day1.objects;

public class Product {
    private String productID;
    private String productName;
    private String productColor;
    private String productSize;
    private int productQuantity;
    private long productPrice;


    public Product() {
    }

    public Product(String productID, String productName, String productColor, String productSize, int productQuantity, long productPrice) {
        this.productID = productID;
        this.productName = productName;
        this.productColor = productColor;
        this.productSize = productSize;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductColor() {
        return productColor;
    }

    public void setProductColor(String productColor) {
        this.productColor = productColor;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public long getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(long productPrice) {
        this.productPrice = productPrice;
    }
}
