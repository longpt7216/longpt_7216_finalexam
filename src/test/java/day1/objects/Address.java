package day1.objects;

public class Address {

    private String name;
    private String telephone;
    private String city;
    private String district;
    private String ward;
    private String address;
    private String adressType;
    private boolean defaultAddress;

    public Address() {
    }

    public Address(String name, String telephone, String city, String district, String ward, String address, String adressType, boolean defaultAddress) {
        this.name = name;
        this.telephone = telephone;
        this.city = city;
        this.district = district;
        this.ward = ward;
        this.address = address;
        this.adressType = adressType;
        this.defaultAddress = defaultAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdressType() {
        return adressType;
    }

    public void setAdressType(String adressType) {
        this.adressType = adressType;
    }

    public boolean isDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }
}
