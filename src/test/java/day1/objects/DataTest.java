package day1.objects;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DataTest {
    String name;
    int age;
    String sex;
    double height;
    double weight;
    String bloodType;
    String work;
    String postNumber;
    String address;
    String mail;
    String telephone;
    String workplace;
    String website;
    int exp;
    String companyID;
    String contractCondition;
    String comment;

    public DataTest() {
    }

    public DataTest(String name, int age, String sex, double height, double weight, String bloodType, String work, String postNumber, String address, String mail, String telephone, String workplace, String website, int exp, String companyID, String contractCondition, String comment) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.height = height;
        this.weight = weight;
        this.bloodType = bloodType;
        this.work = work;
        this.postNumber = postNumber;
        this.address = address;
        this.mail = mail;
        this.telephone = telephone;
        this.workplace = workplace;
        this.website = website;
        this.exp = exp;
        this.companyID = companyID;
        this.contractCondition = contractCondition;
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getPostNumber() {
        return postNumber;
    }

    public void setPostNumber(String postNumber) {
        this.postNumber = postNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getCompanyID() {
        return companyID;
    }

    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    public String getContractCondition() {
        return contractCondition;
    }

    public void setContractCondition(String contractCondition) {
        this.contractCondition = contractCondition;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return name + " ," + age + " ," + sex + " ," + height + " ," + weight + " ," + bloodType + " ," + work + " ," + postNumber
                + " ," + address + " ," + mail + " ," + telephone + " ," + workplace + " ," + website + " ," + exp + " ," +
                companyID + " ," + contractCondition + " ," + comment;

        }
}
