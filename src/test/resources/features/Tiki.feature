@BuyProductOnTikiFullScrenario
Feature: i buy a product on Tiki

  @BuyProductOnTiki
  Scenario: i login to Tiki
    Given i login to Tiki (and clear the Cart if there are products in it)


  Scenario Outline: i add product to cart
    Given i go to "Home" page
    And i go to "Nhà Cửa" page
    When i choose "Trang trí nhà cửa" by the left side menu list
    And i choose "Tranh trang trí" by the left side menu list
    And i choose a product name "<ProductID>"
      | ProductID |
      | product04 |
      | product02 |
    And i choose detail for the product
    Then i add product to Cart
    @Buy3ProductOnTiki
    Examples:
      | ProductID |
      | product01 |
      | product02 |
      | product04 |

  Scenario Outline: i verify product in cart
    Given i go to "Cart" page
    Then i verify the product info and the price is correct
    Given i go to "change address" page from Product page
    When "change" the delivery address to the address of "<Address Name>"
    And i click buy button in Cart page
    Then i verify the address , product info and the price is correctly display in check out page
    @BuyProductOnTiki
    Examples:
      | Address Name |
      | Phạm Thành Long |



#  Flow - 2 Products
#  mvn clean verify -Dwebdriver.driver="chrome" -Dcucumber.filter.tags="@BuyProductOnTiki or @Buy2ProductOnTiki"

#  Flow - 3 Products
#  mvn clean verify -Dwebdriver.driver="chrome" -Dcucumber.filter.tags="@BuyProductOnTiki or @Buy3ProductOnTiki"
