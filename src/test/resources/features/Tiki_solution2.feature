
Feature: i buy a product on Tiki

# BỎ QUA KO LÀM

  Scenario Outline: i add product to cart
    Given i login to Tiki (and clear the Cart if there are products in it)
    Given i go to "Home" page
    And i go to "Nhà Cửa" page
    When i choose "Trang trí nhà cửa" by the left side menu list
    And i choose "Tranh trang trí" by the left side menu list
    And i choose detail for "<Number of Product>" and add to Cart
    Given i go to "Cart" page
    Then i verify the product info and the price is correct
    Given i go to "change address" page from Product page
    When "change" the delivery address to the address of "<Address Name>"
    And i click buy button in Cart page
    Then i verify the address , product info and the price is correctly display in check out page
    Examples:
      | Number of Product | Address Name |
      | 2 | Phạm Thành Long |
      | 3 | Phạm Thành Long |
